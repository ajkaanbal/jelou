import org.scalatest.FlatSpec

class JelouSpec extends FlatSpec {
  "A Jelou object" should "say jeloü" in {
    assert(Jelou.sayIt == "jeloü")
  }
}
